<?php
return [
    'id' => 'ID',
    'stopped_distance' => 'Metros por ping',
    'stopped_pings' => 'Cantidad de pings',
    'shutdown_minutes' => 'Minutos apagado el GPS',
];