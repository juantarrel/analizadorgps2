<?php
return [
    'id'        => 'Ruta',
    'name_file' => 'Nombre del archivo',
    'lat_len'   => 'Latitud/Longitud',
    'date'      => 'Fecha',
    'speed'     => 'Velocidad',
    'distance'  => 'Distancia',
    'battery'   => 'Bateria',
    'location'  => 'Locacion',
    'status'    => 'Estatus',
    'truck_id'  => Yii::t('models/Truck', 'id'),
];