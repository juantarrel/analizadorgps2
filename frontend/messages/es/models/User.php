<?php
return [
	'id'       => 'Usuario',
	'name'     => 'Nombre completo',
	'username' => 'Usuario',
	'password' => 'Contraseña',
	'email'    => 'Correo electrónico',
	'type'     => 'Tipo de usuario',
	'active'   => 'Activo',
];