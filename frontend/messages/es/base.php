<?php
return [
	'Access'     => 'Ingresar',
	'Create new' => 'Crear nuevo',
    /*
    * Modules 
    */
	'Users'        => 'Usuarios',		
	'User'         => 'Usuario',		
	'Routes'       => 'Rutas',
	'Alert Email'  => 'Alerta por Correo',
	'Alerts Email' => 'Alertas por Correo',
	'Alerts'       => 'Alertas',
	'Trucks'       => 'Camiones',
	'Truck'        => 'Camion',
	'Settings'     => 'Configuraciones',
    /*
    * BUTTON
    */
	'Logout'   => 'Salir',
	'Login'    => 'Entrar',
	'Create'   => 'Crear',
	'Create {modelClass}'   => 'Crear {modelClass}',
	'Save'     => 'Guardar',
	'Delete'   => 'Eliminar',
	'Update'   => 'Actualizar',
	'Cancel'   => 'Cancelar',
	'Home'     => 'Inicio',
	'Patients' => 'Pacientes',
	'Diary'    => 'Agenda',
	'Cash'     => 'Caja',
	'Back'     => 'Volver',
	'Active'   => 'Activo',
	'Inactive' => 'Inactivo',
	'Search'   => 'Buscar',
    /*
     * LABEL
     */
	'Select an option'                         => 'Seleccione una opción',
	'user/password invalid'                    => 'Usuario y/o contraseña incorrectos.',
	'user is inactive'                         => 'El usuario esta desactivado.',
	'option {num}'                             => 'Opción {num}',   
	'search input'                             => 'Buscar ...',
	'created date'                             => 'Creado',
	'No results'                               => 'Sin resultados',
	'users_extend'                             => 'Usuario(s)',
	'Without email'                            => 'Sin email',
	'Displaying'                               => 'Mostrando',
	'of'                                       => 'de',
	'Fields with'                              => 'Campos con',
	'are required'                             => 'son requeridos',
	'User is Inactive'                         => 'Usuario esta inactivo',
	'User is Active'                           => 'Usuario esta activo',
	'Are you sure want to active the user?'    => '¿Esta seguro que desea activar el usuario?',
	'Are you sure want to desactive the user?' => '¿Esta seguro que desea desactivar el usuario?',
	'If you forgot your password you can'      => 'Si olvida su contraseña puede',
	'reset it'							       => 'reestablecerla',
	'Analizer GPS'							   => 'Analizador GPS',
    /*
    * FOOTER
    */ 
    //'copyright' => date('Y') . ' by ' . Yii::app()->params['company'],
];