<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AlertEmailModel */

$this->title = Yii::t('base','Create').' '.Yii::t('base','Alert Email');
$this->params['breadcrumbs'][] = ['label' => Yii::t('base','Alert Email'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alert-email-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
