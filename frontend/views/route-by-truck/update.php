<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RouteByTruckModel */

$this->title = 'Update Route By Truck Model: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Route By Truck Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="route-by-truck-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
