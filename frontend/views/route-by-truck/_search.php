<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\TruckModel;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\search\RouteByTruck */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="route-by-truck-model-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

        <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
            //'language' => 'ru',
            //'dateFormat' => 'yyyy-MM-dd',
        ]) ?>


    <?php // echo $form->field($model, 'battery') ?>

    <?php // echo $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php $dataList=ArrayHelper::map(TruckModel::find()->asArray()->all(), 'id', 'name');?>
        <?=$form->field($model, 'truck_id')->dropDownList($dataList, 
                 ['prompt'=>'-Selecciona un camion-',]) ?>

    <?php // echo $form->field($model, 'truck_external_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('base','Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
