<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RouteByTruckModel */

$this->title = 'Create Route By Truck Model';
$this->params['breadcrumbs'][] = ['label' => 'Route By Truck Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="route-by-truck-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
