<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\RouteByTruck */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Route By Truck Models';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="route-by-truck-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>

<?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'lat_len',
            'date',
            'speed',
            'distance',
            // 'battery',
            // 'location',
             'status',
             'truck_id',
            // 'truck_external_id',

        ],
    ]); ?>
<?php \yii\widgets\Pjax::end(); ?>
</div>
