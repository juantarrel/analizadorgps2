<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RouteByTruckModel */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Route By Truck Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="route-by-truck-model-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'lat_len',
            'date',
            'speed',
            'distance',
            'battery',
            'location',
            'status',
            'truck_id',
            'truck_external_id',
        ],
    ]) ?>

</div>
