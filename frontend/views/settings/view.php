<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SettingsModel */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Settings Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-model-view">

    <h1><?= Yii::t('base','Settings') ?></h1>

    <p>
        <?= Html::a(Yii::t('base','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'stopped_distance',
            'stopped_pings',
            'shutdown_minutes',
        ],
    ]) ?>

</div>
