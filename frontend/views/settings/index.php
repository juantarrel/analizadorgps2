<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Settings Models';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-model-index">

    <h1><?= Yii::t('base','Settings') ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'stopped_distance',
            'stopped_pings',
            'shutdown_minutes',

            ['class' => 'yii\grid\ActionColumn',
             'template' => '{view} {update}',
            ],
        ],
    ]); ?>

</div>
