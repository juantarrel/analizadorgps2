<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SettingsModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'stopped_distance')->textInput() ?>

    <?= $form->field($model, 'stopped_pings')->textInput() ?>

    <?= $form->field($model, 'shutdown_minutes')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : Yii::t('base','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
