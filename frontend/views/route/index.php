<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use common\models\TruckModel;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\RouteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//= $form->field($modelCSV, 'from_date')->widget(\yii\jui\DatePicker::classname(), [
    //'language' => 'ru',
    //'dateFormat' => 'yyyy-MM-dd',
//]) 


/*
<?php $form = ActiveForm::begin([
    'id' => 'csv-form',
    'options' => ['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
]) ?>

<?= $form->field($modelCSV, 'filecsv')->fileInput() ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Subir', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>
*/
$this->title = Yii::t('models/Route', 'id');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="route-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
<div class="row">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="col-xs-6 col-sm-4">
        <?php $form = ActiveForm::begin([
            'id' => 'query-form',
            'options' => ['class' => 'form-horizontal', ],
        ]) ?>
        <?php $modelCSV->from_date=isset( $_GET['truck_id'] )? date('d/m/Y') : $yesterdayDate;?>
        <?= $form->field($modelCSV, 'from_date')->widget(DatePicker::classname(), [
            //'language' => 'ru',
            //'dateFormat' => 'yyyy-MM-dd',
        ]) ?>

        <?php
            if( isset( $_GET['truck_id'] ) ){
                $modelCSV->truck=TruckModel::find()->where(['id'=>$_GET['truck_id']])->one()->name;
            }else{
                $modelCSV->truck=  '';
            }
        ?>
        <?php $dataList=ArrayHelper::map(TruckModel::find()->asArray()->all(), 'name', 'name');?>
        <?=$form->field($modelCSV, 'truck')->dropDownList($dataList, 
                 ['prompt'=>'-Selecciona un camion-',]) ?>

            <div class="form-group">
                <div class="col-lg-offset-1 col-lg-11">
                    <?= Html::submitButton('Consultar', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end() ?>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-8">
        
        <div class="container-fluid" id="div-suspected">
            
        </div>
    </div>
</div>
<hr>


    <style>
      #map-canvas {
        height: 600px;
        width: 100%;
        padding: 0px;
        overflow: none;
      }
    </style>


<script src="http://maps.googleapis.com/maps/api/js?v=3&sensor=false"></script>
<script>
(function(window, undefined){

    divSuspected = document.getElementById('div-suspected');
    var map = '';
    function initialize() {
      var mapOptions = {
        zoom: 12,
        center: new google.maps.LatLng(<?= $middlePointX; ?>, <?= $middlePointY; ?>),
        mapTypeId: google.maps.MapTypeId.HYBRID
      };

      map = new google.maps.Map(document.getElementById('map-canvas'),
          mapOptions);
        var json = (function () { 
            var json = null; 
            $.ajax({ 
                'async': false, 
                'global': false, 
                'url': "/route/geodata", 
                'dataType': "json", 
                'success': function (data) {
                    json = data; 
                }
            });
            return json;
        })();

        if( json != null){

          var flightPlanCoordinates = [];
            for (var i = 0, length = json.length; i < length; i++) {
                var data = json[i];
                flightPlanCoordinates.push(new google.maps.LatLng(data.lat, data.len));
            }


          var flightPath = new google.maps.Polyline({
            path: flightPlanCoordinates,
            geodesic: false,
            strokeColor: '#00FF00',
            strokeOpacity: 1.0,
            strokeWeight: 2
          });

          flightPath.setMap(map);
       }else{
            document.getElementById('map-canvas').style.display = 'none';
       }
    

// Construct the circle for each value in citymap.
  // Note: We scale the area of the circle based on the population.
    var jsonSuspected = (function () { 
        var jsonSuspected = null; 
        $.ajax({ 
            'async': false, 
            'global': false, 
            'url': "/route/suspected", 
            'dataType': "json", 
            'success': function (data) {
                jsonSuspected = data; 
            }
        });
        return jsonSuspected;
    })();
    //var flightPlanCoordinatesSuspected = [];
    if( jsonSuspected != null){
        if( jsonSuspected.length > 0){
            pointWhitSuspected = '<div class="alert alert-danger" role="alert" id="pointsSospected"><h5><?=$truckName;?> <?=$routeDate;?></h5>Puntos con Sospecha<br>';
            for (var i = 0, length = jsonSuspected.length; i < length; i++) {
                var data = jsonSuspected[i];
                
                pointWhitSuspected += data.id+'.-<a onclick="centerMapToPont('+data.lat+','+data.len+')" href="#">'+data.lat+','+data.len+'</a><br>';
                //flightPlanCoordinatesSuspected.push(new google.maps.LatLng(data.lat, data.len));
                 var populationOptions = {
                  strokeColor: '#FF0000',
                  strokeOpacity: 0.8,
                  strokeWeight: 2,
                  fillColor: '#FF0000',
                  fillOpacity: 0,
                  map: map,
                  center: new google.maps.LatLng(data.lat, data.len),
                  radius: Math.sqrt(10) * 100
                };
                // Add the circle for this city to the map.
                cityCircle = new google.maps.Circle(populationOptions);
            }

            divSuspected.innerHTML = pointWhitSuspected+"</div>";

        }else{
            divSuspected.innerHTML ='<div class="alert alert-success" role="alert" id="pointsSospected"><?=$truckName;?> <?=$routeDate;?></h5> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Sin sospecha</div>';
        }
    }
    // donde obtienes la latitud y longitud
    //  fromLatLngToPoint("31.054712,-110.898137");
    
    window.centerMapToPont = function(lat,len){
        var b = new google.maps.LatLng(lat,len);
        // map ya se encuentra definido y creado
        map.setZoom(16);
        // aqui es donde haces el centrado
        map.setCenter(b);
    }
    //centerMapToPont(31.054712,-110.898137);
    
    if(!isNaN( $("#uploadcsvform-truck").val().split("-")[1]) &&  $("#pointsSospected").find("h5").text().split("-")[1].split(" ")[0] != $("#uploadcsvform-truck").val().split("-")[1] ){
        $("#query-form").submit();
    }   
}
    google.maps.event.addDomListener(window, 'load', initialize);

})(window);

</script>

<div class="span10" id="map-canvas"></div>
<?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $model,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'name_file',
        [
                'header' => Yii::t('models/Route','lat_len'),
                'format' => 'raw',
                'value'=>  function($data){
                    return '<a target="_blank" href="http://maps.google.com/maps?q='.$data->lat_len.'&iwloc=A&hl=en&t=k&z=20">'.$data->lat_len.'</a>';
                },

            ],            
            'date',
            'speed',
            'distance',
            'battery',
            //'location',
            'status',
            //'truck_id',

        ],
    ]); ?>
<?php \yii\widgets\Pjax::end(); ?>
</div>
