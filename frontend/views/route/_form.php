<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RouteModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="route-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_file')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'lat_len')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'speed')->textInput() ?>

    <?= $form->field($model, 'distance')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'battery')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'truck_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
