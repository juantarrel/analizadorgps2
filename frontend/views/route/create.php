<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RouteModel */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Route Model',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Route Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="route-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
