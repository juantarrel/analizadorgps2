<?php
/* @var $this yii\web\View */
use common\models\TruckModel;
$truckModels = TruckModel::find()->orderBy('status')->all();
?>

<h3><div id="timer"></div></h3>
<?php
foreach( $truckModels as $truck){
?>
<div class="col-md-3">
	<div class="panel panel-primary">
	  <div class="panel-heading">
	    <h3 class="panel-title"><a href="/route-by-truck/index?RouteByTruck[truck_id]=<?=$truck->id?>"><?=$truck->name?></a></h3>
	  </div>
	  <div id="message-<?=$truck->id?>" class="panel-body">
	    <div class="alert alert-success" role="alert">Todo esta en orden</div>
	  </div>
	</div>
	
</div>

<?php
}
?>
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?v=3&sensor=false"></script>
<script>
(function(window, undefined){
	$timer = $('#timer');
    var jsonSuspectedF = function () { 
        var jsonSuspected = null; 
        $.ajax({ 
            'async': false, 
            'global': false, 
            'url': "/route/suspected?byTruck=1", 
            'dataType': "json", 
            'success': function (data) {
                jsonSuspected = data; 
            }
        });
        return jsonSuspected;
    };
    var liveSuspected = function(){
	     jsonSuspected = jsonSuspectedF();
	    
	    if( jsonSuspected != null){
	            for (var i = 0, length = jsonSuspected.length; i < length; i++) {
	                var data = jsonSuspected[i];
	                $("#message-"+data.truck_id).html('<div class="alert alert-danger" role="alert"><span class="parpadea text"><a href="/route/index?truck_id='+data.truck_id+'"&submitH=1" target="_blank">SOSPECHOSO</a></span></div>');
	            }

	    }
    };
    liveSuspected();
    setInterval(function(){ liveSuspected(); }, 10000);


	var date_time = function(id){
        date = new Date;
        year = date.getFullYear();
        month = date.getMonth();
        months = new Array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Augosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
        d = date.getDate();
        day = date.getDay();
        days = new Array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
        h = date.getHours();
        if( h < 10){
                h = "0"+h;
        }
        m = date.getMinutes();
        if( m < 10 ){
                m = "0"+m;
        }
        s = date.getSeconds();
        if( s < 10){
                s = "0"+s;
        }
        result = ''+days[day]+' '+months[month]+' '+d+' '+year+' '+h+':'+m+':'+s;
        document.getElementById(id).innerHTML = result;
        //return true;
	};
        setInterval(function(){date_time('timer');},1000);
	//date_time('timer');
})(window);

</script>
<style>
	.text {
  text-transform:uppercase;
}
.parpadea {
  
  animation-name: parpadeo;
  animation-duration: 1s;
  animation-timing-function: linear;
  animation-iteration-count: infinite;

  -webkit-animation-name:parpadeo;
  -webkit-animation-duration: 1s;
  -webkit-animation-timing-function: linear;
  -webkit-animation-iteration-count: infinite;
}

@-moz-keyframes parpadeo{  
  0% { opacity: 1.0; }
  50% { opacity: 0.0; }
  100% { opacity: 1.0; }
}

@-webkit-keyframes parpadeo {  
  0% { opacity: 1.0; }
  50% { opacity: 0.0; }
   100% { opacity: 1.0; }
}

@keyframes parpadeo {  
  0% { opacity: 1.0; }
   50% { opacity: 0.0; }
  100% { opacity: 1.0; }
}
</style>