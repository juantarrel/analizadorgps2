<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Truckmodel */

$this->title = Yii::t('base','Create').' '.Yii::t('base','Truck');
$this->params['breadcrumbs'][] = ['label' => 'Truckmodels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="truckmodel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
