<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
        if (!Yii::$app->user->isGuest) {
                NavBar::begin([
                    'brandLabel' => Yii::$app->name,
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-inverse navbar-fixed-top',
                    ],
                ]);
                echo '<ul class="navbar-nav navbar-right nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-user"></i>&nbsp;'.Yii::t('base','Routes') .'<b class="caret"></b>
                    </a>                
                    <ul class="dropdown-menu">
                        <li>                
                            <a href="/route/index"><i class="fa fa-list fa-lg"></i>&nbsp;Ruta por dia</a>
                        </li>
                        <li>                
                            <a href="/live-truck/index"><i class="fa fa-list fa-lg"></i>&nbsp;Rutas por camion</a>
                        </li>                        
                    </ul>                            
                </li>
                <li>
                    <a href="/truck/index"><i class="fa fa-list fa-lg"></i>&nbsp;'.Yii::t('base','Trucks').'</a>
                </li>                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-user"></i> '.Yii::$app->user->identity->username .' <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/settings/index"><i class="fa fa-list fa-lg"></i>Configuraciones</a>
                        </li>                      
                        <li>
                            <a href="/site/logout" data-method="post" tabindex="-1"><i class="fa fa-fw fa-power-off"></i> '.Yii::t('base', 'Logout').'</a>
                        </li>
                    </ul>
                </li>
            </ul>';
                NavBar::end();
            }
        ?>

        <div class="" style="width:95%; margin-left:40px;margin-top:60px;">
        <?= Breadcrumbs::widget([
            //'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container" >
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
