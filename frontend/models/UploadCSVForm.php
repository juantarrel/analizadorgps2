<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadCSVForm extends Model
{
    /**
     * @var UploadedFile file attribute
     */
   // public $filecsv;
    public $from_date;
    public $truck;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           // [['filecsv'], 'file', 'extensions' => '',],
            [['from_date'], 'date','format' => 'd/M/yyyy' ],
            [['truck'], 'string',],
        ];
    }
    public function attributeLabels()
    {
        return [
            'from_date' => Yii::t('models/UploadCSV', 'from_date'),
            'truck'     => Yii::t('models/UploadCSV', 'truck'), 
        ];
    }    
}