<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property integer $stopped_distance
 * @property integer $stopped_pings
 * @property integer $shutdown_minutes
 */
class SettingsModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stopped_distance', 'stopped_pings', 'shutdown_minutes'], 'required'],
            [['stopped_distance', 'stopped_pings', 'shutdown_minutes'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'stopped_distance' => Yii::t('models/Settings','stopped_distance'),
            'stopped_pings'    => Yii::t('models/Settings','stopped_pings'),
            'shutdown_minutes' => Yii::t('models/Settings','shutdown_minutes'),
        ];
    }
}
