<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RouteByTruckModel;

/**
 * RouteByTruck represents the model behind the search form about `app\models\RouteByTruckModel`.
 */
class RouteByTruck extends RouteByTruckModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'speed', 'truck_id'], 'integer'],
            [['lat_len', 'date', 'distance', 'battery', 'location', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RouteByTruckModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'speed' => $this->speed,
            'truck_id' => $this->truck_id,
        ]);

        $query->andFilterWhere(['like', 'lat_len', $this->lat_len])
            ->andFilterWhere(['like', 'distance', $this->distance])
            ->andFilterWhere(['like', 'battery', $this->battery])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
