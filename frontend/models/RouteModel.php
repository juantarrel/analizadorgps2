<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "route".
 *
 * @property integer $id
 * @property string $name_file
 * @property string $lat_len
 * @property string $date
 * @property integer $speed
 * @property string $distance
 * @property string $battery
 * @property string $location
 * @property string $status
 * @property integer $truck_id
 *
 * @property Truck $truck
 */
class RouteModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'route';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_file', 'truck_id'], 'required'],
            [['date'], 'safe'],
            [['speed', 'truck_id'], 'integer'],
            [['name_file', 'lat_len', 'distance', 'battery', 'location'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => Yii::t('models/Route', 'id'),
            'name_file' => Yii::t('models/Route', 'name_file'), 
            'lat_len'   => Yii::t('models/Route', 'lat_len'),
            'date'      => Yii::t('models/Route', 'date'),
            'speed'     => Yii::t('models/Route', 'speed'),
            'distance'  => Yii::t('models/Route', 'distance'),
            'battery'   => Yii::t('models/Route', 'battery'),
            'location'  => Yii::t('models/Route', 'location'),
            'status'    => Yii::t('models/Route', 'status'),
            'truck_id'  => Yii::t('models/Route', 'truck_id'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTruck()
    {
        return $this->hasOne(Truck::className(), ['id' => 'truck_id']);
    }
}
