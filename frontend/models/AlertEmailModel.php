<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alert_email".
 *
 * @property integer $id
 * @property string $email
 * @property integer $status
 */
class AlertEmailModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alert_email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['status'], 'integer'],
            [['email'], 'string', 'max' => 45],
            [['email'], 'unique'],
            [['email'], 'email'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'     => Yii::t('models/AlertEmail', 'id'),
            'email'  => Yii::t('models/AlertEmail', 'email'),
            'status' => Yii::t('models/AlertEmail', 'status'),
        ];
    }
}
