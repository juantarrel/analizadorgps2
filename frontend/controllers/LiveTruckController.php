<?php

namespace frontend\controllers;
use common\models\TruckModel;

class LiveTruckController extends \yii\web\Controller
{
    public function actionGetTruckData()
    {
        return $this->render('get-truck-data');
    }

    public function actionIndex()
    {

        return $this->render('index');
    }

}
