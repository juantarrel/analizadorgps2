<?php

namespace frontend\controllers;

use Yii;
use app\models\RouteModel;
use common\models\TruckModel;
use common\models\RouteByTruckModel;
use app\models\SettingsModel;
use app\models\search\RouteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadCSVForm;
use yii\web\UploadedFile;
use yii\helpers\Json;
use yii\filters\AccessControl;
use yii\web\Request;
/**
 * RouteController implements the CRUD actions for RouteModel model.
 */
class RouteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','suspected','geodata','helloworld'],
                'rules' => [
                    [
                        'actions' => ['index','suspected','geodata'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],                   
                ],
            ],        
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {

        $searchModel = new RouteSearch();
        $searchModel->setAttributes([]); 
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $modelCSV = new UploadCSVForm();

        if (Yii::$app->request->isPost) {

            $modelCSV->load ( Yii::$app->request->post() );
            if( $modelCSV->validate() ){

                $dateQuery = explode( '/', $modelCSV->from_date );
                $dateQuery = $dateQuery[1].'/'.$dateQuery[0].'/'.$dateQuery[2] ;
                $arData = Yii::$app->parserGPS->parse( $truckName = $modelCSV->truck, $date = $dateQuery.'%2012:00:00%20AM' ); 
                
                if( is_array( $arData ) && $arData != null  ){

                    RouteModel::deleteAll('id>0');
                    Yii::$app->db->createCommand()->truncateTable( RouteModel::tableName() );

                    $truckModel = TruckModel::find()->where(['name' => $truckName])->one();
                    $truckId = $truckModel->id;
                    /*if( isset( $truckModel->id ) ){
                        $truckId = $truckModel->id;
                    }else{
                        $truckModel = new TruckModel();
                        $truckModel->name = $truckName;
                        if( $truckModel->save() ){
                            $truckId = $truckModel->id;
                        }
                    } */
                    
                    for ($row = 0; $row < count( $arData ) -1 ; $row++) {
                        $modelNewRoute = new RouteModel();
                        $modelNewRoute->truck_id  = $truckId;
                        $modelNewRoute->name_file = '11'; 
                        $modelNewRoute->date      = date('Y-m-d h:i:s A', strtotime( $arData[$row][2] ) ) ;
                        $modelNewRoute->status    = $arData[$row][3]; //comentario
                        $modelNewRoute->location  = $arData[$row][4]; //comentario
                        $modelNewRoute->battery   = $arData[$row][5]; //comentario
                        $modelNewRoute->lat_len   = $arData[$row][6]; //latitud
                        $modelNewRoute->speed     = $arData[$row][7]; //velocidad
                        $modelNewRoute->distance  = $arData[$row][8]; //distance
                        $modelNewRoute->save();
                    }
                }
            }
            /*
            $modelCSV->filecsv = UploadedFile::getInstance($modelCSV, 'filecsv');
            if ($modelCSV->filecsv ){ //&& $modelCSV->validate()) {   
                $originalName = $modelCSV->filecsv->name;
                $truckName = explode( '_', $originalName )[0];
                $fileName = explode('.', $originalName)[0];

                $file = UploadedFile::getInstance($modelCSV, 'filecsv');

                $truckModel = TruckModel::find( 'name="'.$truckName.'"' );
                if( isset($truckModel->id) ){
                    //print_r($truckModel);exit();
                    $truckId = $truckModel->id;
                }else{
                    $truckModel = new TruckModel();
                    $truckModel->name = $truckName;
                    if( $truckModel->save() ){
                        $truckId = $truckModel->id;
                    }
                }   
                RouteModel::deleteAll('id>0');
                Yii::$app->db->createCommand()->truncateTable( RouteModel::tableName() );
                $fp = fopen($file->tempName, 'r');
                if( $fp ){
                    $first_time = 0;
                    do{
                        if( $first_time  <=2 || !is_numeric( $line[0] ) ) {
                            $first_time++;
                            continue;
                        }
                        $modelNewRoute = new RouteModel();
                        
                        $modelNewRoute->truck_id  = $truckId;
                        $modelNewRoute->name_file = '11'; 
                        $modelNewRoute->lat_len   = $line[6]; //latitud
                        $modelNewRoute->date      = date('Y-m-d h:i:s A', strtotime( $line[2] ) ); //fecha
                        $modelNewRoute->speed     = $line[7]; //velocidad
                        $modelNewRoute->distance  = $line[8]; //distance
                        $modelNewRoute->battery   = $line[5]; //comentario
                        $modelNewRoute->location  = $line[4]; //comentario
                        $modelNewRoute->status    = $line[3]; //comentario
                        $modelNewRoute->save();

                    }while( ($line = fgetcsv($fp, 1000, ",")) != FALSE);

                }
            }
            */
        }

        $yesterdayDate = date('d/m/Y',strtotime("-1 days"));

        $modelSuspected = new RouteModel( ); 
        
        $truckTheName = '';
        $middlePointX = 0;
        $middlePointY = 0;
        $routeDate = '';
        $routesModel = RouteModel::find()->one();
        if( isset( $routesModel->id ) ){
            $idRoute =  $routesModel->truck_id;
            $truckTheName = TruckModel::find()->where(['id'=>$idRoute])->one()->name;

            $firstPoint = RouteModel::find()->limit(1)->one()->lat_len;
            $routeDate = date( 'd/m/Y' , strtotime( RouteModel::find()->limit(1)->one()->date ) );
            
            $firstPoint = explode( ',', $firstPoint );

            $sql='SELECT * FROM '.RouteModel::tableName().' ORDER BY id DESC limit 1';
            
            $lastPoint=RouteModel::findBySql($sql)->all()[0]['lat_len'];

            $lastPoint = explode( ',', $lastPoint );
            $middlePoint = Yii::$app->geodesica->middlePoint( $firstPoint[0], $firstPoint[1], $lastPoint[0], $lastPoint[1] );
            $middlePointX = explode( ',', $middlePoint )[0];
            $middlePointY = explode( ',', $middlePoint )[1];
        }

        return $this->render('index', [
            'searchModel'   => $searchModel,
            'model'         => $dataProvider,
            'modelCSV'      => $modelCSV,
            'truckName'     => $truckTheName,
            'middlePointX'  => $middlePointX,
            'middlePointY'  => $middlePointY,
            'yesterdayDate' => $yesterdayDate,
            'routeDate' => $routeDate,
        ]);
    }

    public function actionSuspected(){
        $byTruck = isset( $_GET['byTruck']) ? $_GET['byTruck'] : '';
        
       /* if( $byTruck == ''){
            $modelAll = RouteModel::find()->all();
        }else{*/
            $sql      ='SELECT * FROM '.RouteByTruckModel::tableName().' WHERE DATE(`date`) = CURDATE() ';
            $modelAll = RouteByTruckModel::findBySql($sql)->all();
            //$modelAll = json_decode(json_encode($modelAll), FALSE);
            //$modelAll = RouteByTruckModel::find()->where(['truck_id'=>$truck_id,'date'=>date('d/m/Y')])->all();
        //}
        $settingsModel = SettingsModel::find()->where(['id'=>'1'])->one();
        $stoppedDistance = $settingsModel->stopped_distance;
        $stoppedPings = $settingsModel->stopped_pings;
        $shutdownMinutes = $settingsModel->shutdown_minutes;
        $arrayLatLen = [];
        if( $modelAll ){
            $count = 0;
            $tmpLat = 0;
            $tmpLen = 0;
            $startDate = 0;
            $possibleSuspected = 0;
            foreach( $modelAll as $key){
                if( $count == 0){
                    $tmpLat    = explode( ',', $key->lat_len )[0];
                    $tmpLen    = explode( ',', $key->lat_len )[1];
                    $startDate = $key->date;
                }
                $truckId = $key->truck_id;
                $currentLat = explode( ',', $key->lat_len )[0];
                $currentLen = explode( ',', $key->lat_len )[1];
                $endDate   = $key->date ;
                if( $tmpLat !=  $currentLat && $tmpLen != $currentLen && $startDate != $endDate ){
                    $distance = Yii::$app->geodesica->distance( $tmpLat, $tmpLen, $currentLat, $currentLen) ;

                    /*$datetime1 = strtotime($startDate);
                    $datetime2 = strtotime($endDate);
                    $interval  = abs($datetime2 - $datetime1);
                    $minutes   = intval($interval/60);

                    $datetime1 = new \DateTime($startDate);
                    $datetime2 = new \DateTime($endDate);
                    $interval = $datetime1->diff($datetime2);
                    $minutes = $interval->format('%i');*/

                    $t1 = strtotime($startDate);
                    $t2 = strtotime($endDate);
                    $delta_T = ($t2 - $t1);
                    $minutes = round(((($delta_T % 604800) % 86400) % 3600) / 60); 


                    if( $minutes >= $shutdownMinutes ){                      
                        if( !in_array( $tmpLat.','.$tmpLen, [$arrayLatLen] ) ){
                            $arrayLatLen[] = [

                                    'id'       => $count+1,
                                    'lat'      => $tmpLat,
                                    'len'      => $tmpLen,
                                    'truck_id' => $truckId,
                                ];

                        }
                    }

                    if( $distance < $stoppedDistance/100000  ){
                        if( $possibleSuspected >= $stoppedPings){
                            if( !in_array( $tmpLat.','.$tmpLen, [$arrayLatLen] ) ){
                                $arrayLatLen[] = [
                                        'id'       => $count+1,
                                        'lat'      => $tmpLat,
                                        'len'      => $tmpLen,
                                        'truck_id' => $truckId,
                                    ];

                            }
                        }
                        $possibleSuspected++;
                    }else{
                $possibleSuspected = 0;

                    }
                }
                $tmpLat = $currentLat;
                $tmpLen = $currentLen;
                

                
                $startDate = $endDate;
                $count++;
            }
        }
        echo Json::encode( array_values( array_unique( $arrayLatLen, SORT_REGULAR ) ) );
    }

    public function actionGeodata()
    {
        $modelAll = RouteModel::find()->all();
        $arrayLatLen = [];
        if( $modelAll ){
            $last_key = end(array_keys($modelAll));
            foreach( $modelAll as $key){
            
                $arrayLatLen[] = [
                        'lat' => explode( ',', $key->lat_len )[0] ? explode( ',', $key->lat_len )[0] : '',
                        'len' => explode( ',', $key->lat_len )[1] ? explode( ',', $key->lat_len )[1] : '',
                        
                    ];
            
            }
        }
        echo Json::encode($arrayLatLen);
    }

    /**
     * Finds the RouteModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RouteModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RouteModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
