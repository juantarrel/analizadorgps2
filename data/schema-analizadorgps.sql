SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema amadotrk_angps
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `amadotrk_angps` DEFAULT CHARACTER SET latin1 ;
USE `amadotrk_angps` ;

-- -----------------------------------------------------
-- Table `amadotrk_angps`.`truck`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `amadotrk_angps`.`truck` ;

CREATE TABLE IF NOT EXISTS `amadotrk_angps`.`truck` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `status` INT(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 26
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amadotrk_angps`.`route`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `amadotrk_angps`.`route` ;

CREATE TABLE IF NOT EXISTS `amadotrk_angps`.`route` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name_file` VARCHAR(100) NOT NULL,
  `lat_len` VARCHAR(100) NULL DEFAULT NULL,
  `date` TIMESTAMP NULL DEFAULT NULL,
  `speed` INT(45) NULL DEFAULT '0',
  `distance` VARCHAR(100) NULL DEFAULT NULL,
  `battery` VARCHAR(100) NULL DEFAULT NULL,
  `location` VARCHAR(100) NULL DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  `truck_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_route_truck_idx` (`truck_id` ASC),
  CONSTRAINT `fk_route_truck`
    FOREIGN KEY (`truck_id`)
    REFERENCES `amadotrk_angps`.`truck` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3745
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amadotrk_angps`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `amadotrk_angps`.`user` ;

CREATE TABLE IF NOT EXISTS `amadotrk_angps`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `username` VARCHAR(30) NOT NULL,
  `password` VARCHAR(72) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `auth_key` VARCHAR(80) NOT NULL,
  `password_reset_token` VARCHAR(80) NULL DEFAULT NULL,
  `password_hash` VARCHAR(80) NULL DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  `created_at` INT(11) NULL DEFAULT NULL,
  `updated_at` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username` (`username` ASC),
  UNIQUE INDEX `email` (`email` ASC),
  INDEX `id` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `amadotrk_angps`.`alert_email`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `amadotrk_angps`.`alert_email` ;

CREATE TABLE IF NOT EXISTS `amadotrk_angps`.`alert_email` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `status` INT(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `amadotrk_angps`.`settings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `amadotrk_angps`.`settings` ;

CREATE TABLE IF NOT EXISTS `amadotrk_angps`.`settings` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `stopped_distance` INT(11) NOT NULL,
  `stopped_pings` INT(11) NOT NULL,
  `shutdown_minutes` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `amadotrk_angps`.`settings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `amadotrk_angps`.`settings` ;

CREATE TABLE IF NOT EXISTS `amadotrk_angps`.`settings` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `stopped_distance` INT(11) NOT NULL,
  `stopped_pings` INT(11) NOT NULL,
  `shutdown_minutes` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `amadotrk_angps`.`route_by_truck`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `amadotrk_angps`.`route_by_truck` ;

CREATE TABLE IF NOT EXISTS `amadotrk_angps`.`route_by_truck` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `lat_len` VARCHAR(100) NULL,
  `date` TIMESTAMP NULL,
  `speed` INT(45) NULL,
  `distance` VARCHAR(100) NULL,
  `battery` VARCHAR(100) NULL,
  `location` VARCHAR(100) NULL,
  `status` VARCHAR(45) NULL,
  `truck_id` INT(11) NOT NULL,
  `truck_external_id` INT(11) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_route_by_truck_truck1_idx` (`truck_id` ASC),
  CONSTRAINT `fk_route_by_truck_truck1`
    FOREIGN KEY (`truck_id`)
    REFERENCES `amadotrk_angps`.`truck` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
