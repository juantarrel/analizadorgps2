<?php
 
namespace console\controllers;
 
use yii\console\Controller;
use yii;
use common\models\TruckModel;
use common\models\RouteByTruckModel;
/**
 * Live controller
 */
class LiveController extends Controller {
 
    public function actionIndex() {
        echo "cron service runnning";
        $truckModel = Truckmodel::find()->where(['status'=>'1'])->all();
        $arData = [];
        foreach ($truckModel as $truck) {
            $dateQuery = explode( '/', date( 'd/m/Y' ) );
            $dateQuery = $dateQuery[1].'/'.$dateQuery[0].'/'.$dateQuery[2] ;
            $arData = Yii::$app->parserGPS->parse( $truckName = $truck->name, $date = $dateQuery.'%2012:00:00%20AM' ); 
            if( count( $arData ) && is_array( $arData ) ){
                
                for ($row = 0; $row < count( $arData ) -1 ; $row++) {
                    if( !isset( RouteByTruckModel::find()->where(
                        [
                            'truck_external_id' => $arData[$row][0],
                            'truck_id'          => $truck->id,
                            'date'              => date('Y-m-d h:i:s A', strtotime( $arData[$row][2] ) ),
                        ]
                        )->one()->id ) ){
                        $modelNewRoute                    = new RouteByTruckModel();
                        $modelNewRoute->truck_id          = $truck->id;
                        $modelNewRoute->truck_external_id = $arData[$row][0];
                        $modelNewRoute->date              = date('Y-m-d h:i:s A', strtotime( $arData[$row][2] ) ) ;
                        $modelNewRoute->status            = $arData[$row][3]; //comentario
                        $modelNewRoute->location          = $arData[$row][4]; //comentario
                        $modelNewRoute->battery           = $arData[$row][5]; //comentario
                        $modelNewRoute->lat_len           = $arData[$row][6]; //latitud
                        $modelNewRoute->speed             = $arData[$row][7]; //velocidad
                        $modelNewRoute->distance          = $arData[$row][8]; //distance
                        $modelNewRoute->save();

                    }
                }                
            }
            //date('d/m/Y');
        }
    } 
}