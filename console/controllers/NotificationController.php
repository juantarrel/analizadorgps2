<?php
 
namespace console\controllers;
 
use yii\console\Controller;
use yii;
 
/**
 * Notification controller
 */
class NotificationController extends Controller {
 
    public function actionIndex() {
        echo "cron service runnning";
    }
 
    public function actionMail() {
        echo "Sending mail to ";
        Yii::$app->mailer->compose('suspectedPing-html', ['content' => 'Que pedo morro'])
             ->setFrom('juantarrel@gmail.com')
		     ->setTo('juantarrel@gmail.com')
		     ->setSubject('Analizador GPS')
		     ->send();
    }
 
}