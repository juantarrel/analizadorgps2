<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class ParserHTMLGPS extends Component {
//http://199.217.119.209/lpoint.aspx?name=TRUCK-80&checkbox1=true&calDate=2/3/2015%2012:00:00%20AM
	private $urlGPS = 'http://199.217.119.209/lpoint.aspx';
	private $checkbox1 = 'checkbox1=true';

	private function curl($vars){
		
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->urlGPS);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);

		curl_close ($ch);

		return $server_output;
	}
	public function parse($truck, $date){
	 	$vars =  'name='.$truck.'&'.$this->checkbox1.'&calDate='.$date.'';
	 	$htmlContent =  $this->curl($vars);

	    $DOM = new \DOMDocument();
	    @$DOM->loadHTML( $htmlContent );

	    $items = $DOM->getElementsByTagName('tr');


	    $arTr = [];	
	    foreach ($items as $node){
	    	if( $this->tdrows( $node->childNodes ) != null){
	    		$arTr[] = $this->tdrows( $node->childNodes );
			}
	    }
	    return $arTr;
	}
	public function tdrows($elements){
        $count = 0;
        $arTd = [];
        $firstElement = true;
        foreach ($elements as $element){
        	$nodeValue = trim( $element->nodeValue );
	        if( $count == 0 && ( $nodeValue == '' || $nodeValue == '#' ) ){
	        	return null;
			}
			$arTd[] = $nodeValue;
			$count++;
        }
        return $arTd;
    }
}