<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "route_by_truck".
 *
 * @property integer $id
 * @property string $lat_len
 * @property string $date
 * @property integer $speed
 * @property string $distance
 * @property string $battery
 * @property string $location
 * @property string $status
 * @property integer $truck_id
 * @property integer $truck_external_id
 *
 * @property Truck $truck
 */
class RouteByTruckModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'route_by_truck';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['speed', 'truck_id', 'truck_external_id'], 'integer'],
            [['truck_id'], 'required'],
            [['lat_len', 'distance', 'battery', 'location'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lat_len' => 'Lat Len',
            'date' => 'Date',
            'speed' => 'Speed',
            'distance' => 'Distance',
            'battery' => 'Battery',
            'location' => 'Location',
            'status' => 'Status',
            'truck_id' => 'Truck ID',
            'truck_external_id' => 'Truck External ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTruck()
    {
        return $this->hasOne(Truck::className(), ['id' => 'truck_id']);
    }
}
