<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "truck".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 *
 * @property Route[] $routes
 * @property RouteByTruck[] $routeByTrucks
 */
class TruckModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'truck';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['name'], 'integer', ],
            [['name'], 'unique'],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

                $this->name = 'TRUCK-'.$this->name;
            return true;
        } else {

            return false;
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasMany(Route::className(), ['truck_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRouteByTrucks()
    {
        return $this->hasMany(RouteByTruck::className(), ['truck_id' => 'id']);
    }
}
